console.log("Page loaded!")


var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getQuizResults; 




function getQuizResults(){
    console.log('entered function')

    var results = {};

    results["q1"] = false;
    results["q2"] = false;
    results["q3"] = false;
    results["q4"] = false;   
    results["q5"] = false;
    results["q6"] = false;
    results["q7"] = false; 

    var score = 0
    for(var key in results){ 
        selected = document.getElementById(key + '-checked')
        document.getElementById(key + '-div').style.color = "green"
        if(selected.checked){
            console.log(key + '-checked');
            results[key] = true;
            score++;
        }
    }
    
    var resultsScreen = document.getElementById("results-screen");
    resultsScreen.innerHTML = "you scored a " + score + " out of 7";

}


var againButton = document.getElementById('results-button');
againButton.onmouseup = takeQuizAgain; 

function takeQuizAgain(){
    console.log('entered takeQuizAgain')
    location.reload()
}
